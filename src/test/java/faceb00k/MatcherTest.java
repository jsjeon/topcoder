package faceb00k;

import static org.junit.Assert.*;
import org.junit.*;

public class MatcherTest {

  static void test(String pattern, String target, boolean expected) {
    boolean res = Matcher.isMatch(pattern, target);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    test("abc", "abc", true);
    test("abc", "ab", false);
    test("abc", "abcd", false);
  }

  @Test
  public void ex1() {
    test("a?c", "ac", false);
    test("a?c", "abc", true);
    test("a?c", "abbc", false);
  }

  @Test
  public void ex2() {
    test("x*z", "xz", true);
    test("x*z", "xyz", true);
    test("x*z", "xyyyyyyz", true);
  }

  @Test
  public void ex3() {
    test("*", "", true);
    test("*", "abc", true);
    test("**", "", true);
    test("**", "abc", true);
  }

  @Test
  public void ex4() {
    test("?b?d?", "abcde", true);
    test("?b?d?", "bcde", false);
    test("?b?d?", "abde", false);
    test("?b?d?", "abcd", false);
  }

  @Test
  public void ex5() {
    test("a?c*e", "abce", true);
    test("a?c*e", "abcde", true);
    test("a?c*e", "abcdddddde", true);
  }

}

