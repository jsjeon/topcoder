package faceb00k;

/* 
For each node in a binary tree find the next right node on the same depth. Write a function that takes root node and populates "next" with the answer for each node. 

A 
/ \ 
B -> C 
/ / \ 
D -> F-> G 
/ \ 
H -> I 

class Node { 
Node left; 
Node right; 
Node next; // <-- answer should be stored here 
}; 

B.next = C 
D.next = F 
F.next = G 
H.next = I 
{A, C, G, I}.next = null 
*/

import static org.junit.Assert.*;
import org.junit.*;

public class NextRightTest {

  @Test
  public void ex0() {
    NextRight.Node a = new NextRight.Node();

    NextRight.Node b = new NextRight.Node();
    NextRight.Node c = new NextRight.Node();
    a.left = b;
    a.right = c;

    NextRight.Node d = new NextRight.Node();
    b.left = d;

    NextRight.Node f = new NextRight.Node();
    NextRight.Node g = new NextRight.Node();
    c.left = f;
    c.right = g;

    NextRight.Node h = new NextRight.Node();
    NextRight.Node i = new NextRight.Node();
    d.left = h;
    d.right = i;

    NextRight.populateNext(a);

    assertEquals(c, b.next);
    assertEquals(f, d.next);
    assertEquals(g, f.next);
    assertEquals(i, h.next);

    assertNull(a.next);
    assertNull(c.next);
    assertNull(g.next);
    assertNull(i.next);

  }

}

