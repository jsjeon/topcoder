package faceb00k;

import static org.junit.Assert.*;
import org.junit.*;

public class MaxDropTest {

  static void test(int[] arr, int expected) {
    int res = MaxDrop.find(arr);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    int[] arr = new int[] { 6, 5, 4, 3, 2, 1 };
    test(arr, 6 - 1);
  }

  @Test
  public void ex1() {
    int[] arr = new int[] { 100, 80, 90, 40, 50, 10, 20 };
    test(arr, 100 - 10);
  }

  @Test
  public void ex2() { // min elt is not always involved
    int[] arr = new int[] { 80, 20, 10, 100, 20 };
    test(arr, 100 - 20);
  }

  @Test
  public void ex3() { // max elt is not always involved
    int[] arr = new int[] { 90, 20, 10, 100, 30 };
    test(arr, 90 - 10);
  }

}

