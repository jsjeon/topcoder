/*
The "surpasser" of an element in an array is defined as the number of elements that are to the "right" and bigger than itself. 

Example: 
Array: 
[2, 7, 5, 5, 2, 7, 0, 8, 1] 
The "surpassers" are 
[5, 1, 2, 2, 2, 1, 2, 0, 0] 

Question: Find the maximum surpasser of the array. 

In this example, maximum surpasser = 5
*/

package go0g1e;

import static org.junit.Assert.*;
import org.junit.*;

public class SurpasserTest {

  static void test(int[] arr, int expected) {
    int res = Surpasser.max(arr);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    int[] arr = new int[] { 2, 7, 5, 5, 2, 7, 0, 8, 1 };
    test(arr, 5);
  }

  @Test
  public void ex1() {
    int[] arr = new int[] { 8, 7, 6, 5, 4, 3, 2, 1, 0 };
    test(arr, 0);
  }

  @Test
  public void ex2() {
    int[] arr = new int[] { 1, 2, 3, 4, 5 };
    test(arr, 4);
  }

  @Test
  public void ex3() {
    int[] arr = new int[] { 5, 4, 1, 3, 2 };
    test(arr, 2);
  }

}
