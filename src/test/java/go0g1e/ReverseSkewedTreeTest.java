package go0g1e;

import static org.junit.Assert.*;
import org.junit.*;

public class ReverseSkewedTreeTest {

  static void test(ReverseSkewedTree.Node root, String expected) {
    ReverseSkewedTree.Node nRoot = ReverseSkewedTree.reverse(root);
    assertEquals(expected, nRoot.toString());
  }

  /**       a          f
   *       / \        / \
   *      b   c      g   d
   *     / \            / \
   *    d   e    ->    e   b
   *   / \                / \
   *  f   g              c   a
   */
  @Test
  public void ex0() {
    ReverseSkewedTree.Node root = new ReverseSkewedTree.Node('a');
    ReverseSkewedTree.Node n = root;

    n.addChildren('b', 'c');
    n = n.left;

    n.addChildren('d', 'e');
    n = n.left;

    n.addChildren('f', 'g');
    n = n.left;

    test(root, "gfedcba");
  }
}

