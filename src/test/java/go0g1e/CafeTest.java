package go0g1e;

import static org.junit.Assert.*;
import org.junit.*;

public class CafeTest {

  static void testRev(Cafe[] cafes, int expected) {
    int res = Cafe.maxEnjoymentRev(cafes);
    assertEquals(expected, res);
  }

  static void testDP(Cafe[] cafes, int expected) {
    int res = Cafe.maxEnjoymentDP(cafes);
    assertEquals(expected, res);
  }

  @Test
  public void ex0Rev() {
    Cafe[] cafes = new Cafe[4];
    cafes[0] = new Cafe(90, 2);
    cafes[1] = new Cafe(60, 0);
    cafes[2] = new Cafe(60, 1);
    cafes[3] = new Cafe(25, 5);
    testRev(cafes, 120);
  }

  @Test
  public void ex0DP() {
    Cafe[] cafes = new Cafe[4];
    cafes[0] = new Cafe(90, 2);
    cafes[1] = new Cafe(60, 0);
    cafes[2] = new Cafe(60, 1);
    cafes[3] = new Cafe(25, 5);
    testDP(cafes, 120);
  }

}

