package go0g1e;

import static org.junit.Assert.*;
import org.junit.*;

public class QuoteTest {

  static void test(String txt, String expected) {
    String res = Quote.subst(txt);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    String no_quote = "hello, this one doesn_t have any quote";
    test(no_quote, no_quote);
  }

  @Test
  public void ex1() {
    String quoted = "\"it's rainy out there\"";
    test(quoted, "'it's rainy out there'");
  }

  @Test
  public void ex2() {
    String w_both_quotes = "\"you're \"handsome\"!\"";
    test(w_both_quotes, "'you're \"handsome\"!'");
  }

  @Test
  public void ex3() {
    String just_quote = "\"\"\"";
    test(just_quote, "'\"'");
  }

  @Test
  public void ex4() {
    String just_quote = "'\"'";
    test(just_quote, "\"\"\"");
  }

}
