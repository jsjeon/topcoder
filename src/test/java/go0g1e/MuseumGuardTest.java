/*
Imagine a museum floor that looks like this: 

.#.G. 
..#.. 
G.... 
..#.. 

G == Museum Guard 
# == obstruction/impassable obstacle 
. == empty space 

Write a piece of code that will find the nearest guard for each open floor space. Diagonal moves are not allowed. The output should convey this information: 

2#1G1 
12#12 
G1223 
12#34 

You may choose how you want to receive the input and output. For example, you may use a 2-d array, as depicted here, or you may use a list of points with features, if you deem that easier to work with, as long as the same information is conveyed. 
*/

package go0g1e;

import static org.junit.Assert.*;
import org.junit.*;

public class MuseumGuardTest {

  static void testFW(String[] floor, String[] expected) {
    String[] nearest = MuseumGuard.nearestGuardFW(floor);
    assertEquals(nearest.length, expected.length);
    for (int i = 0; i < nearest.length; i++) {
      assertEquals(expected[i], nearest[i]);
    }
  }

  static void testBFS(String[] floor, String[] expected) {
    String[] nearest = MuseumGuard.nearestGuardBFS(floor);
    assertEquals(nearest.length, expected.length);
    for (int i = 0; i < nearest.length; i++) {
      assertEquals(expected[i], nearest[i]);
    }
  }

  @Test
  public void ex0() {
    String[] floor = new String[]
      {".#.G.",
       "..#..",
       "G....",
       "..#.."};
    String[] expected = new String[]
      {"2#1G1",
       "12#12",
       "G1223",
       "12#34"};
    testFW(floor, expected);
    testBFS(floor, expected);
  }

}

