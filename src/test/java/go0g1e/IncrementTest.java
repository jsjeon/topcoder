package go0g1e;

import static org.junit.Assert.*;
import org.junit.*;

public class IncrementTest {

  static void test(int[] arr) {
    int cur = Increment.toInt(arr);
    int[] res = Increment.inc(arr);
    assertEquals(cur+1, Increment.toInt(res));
  }

  @Test
  public void ex0() {
    int[] arr = new int[] { 1, 2, 3 };
    test(arr);
  }

  @Test
  public void ex1() {
    int[] arr = new int[] { 3, 6, 9 };
    test(arr);
  }

  @Test
  public void ex2() {
    int[] arr = new int[] { 9 };
    test(arr);
  }

  @Test
  public void ex3() {
    int[] arr = new int[] { 9, 9 };
    test(arr);
  }

}
