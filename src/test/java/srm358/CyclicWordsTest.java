/*
Problem Statement
    We can think of a cyclic word as a word written in a circle. To represent a cyclic word, we choose an arbitrary starting position and read the characters in clockwise order. So, "picture" and "turepic" are representations for the same cyclic word.

You are given a String[] words, each element of which is a representation of a cyclic word. Return the number of different cyclic words that are represented.

 
Definition

Class:  CyclicWords
Method: differentCW
Parameters: String[]
Returns:    int
Method signature: int differentCW(String[] words)
(be sure your method is public)
    
 
Constraints
- words will contain between 1 and 50 elements, inclusive.
- Each element of words will contain between 1 and 50 lowercase letters ('a'-'z'), inclusive.
 
Examples
0)

{ "picture", "turepic", "icturep", "word", "ordw" }
Returns: 2
"picture", "turepic" and "iceturep" are representations of the same cyclic word. "word" and "ordw" are representations of a second cyclic word.
1)

{ "ast", "ats", "tas", "tsa", "sat", "sta", "ttt" }
Returns: 3
2)

{"aaaa", "aaa", "aa", "aaaa", "aaaaa"}
Returns: 4
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.
*/

package srm358;

import static org.junit.Assert.*;
import org.junit.*;

public class CyclicWordsTest {

  static void testCyclic(String[] words, int expected) {
    int res = CyclicWords.differentCW(words);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    String[] words = new String[] { "picture", "turepic", "icturep", "word", "ordw" };
    testCyclic(words, 2);
  }

  @Test
  public void ex1() {
    String[] words = new String[] { "ast", "ats", "tas", "tsa", "sat", "sta", "ttt" };
    testCyclic(words, 3);
  }

  @Test
  public void ex2() {
    String[] words = new String[] { "aaaa", "aaa", "aa", "aaaa", "aaaaa" };
    testCyclic(words, 4);
  }

}
