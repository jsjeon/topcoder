/*
Problem Statement
    You are given a String[] words. Return the shortest String that contains all the words as substrings. If there are several possible answers, return the one that comes first lexicographically.
 
Definition

Class:  JoinedString
Method: joinWords
Parameters: String[]
Returns:    String
Method signature: String joinWords(String[] words)
(be sure your method is public)

 
Constraints
- words will contain between 1 and 12 elements, inclusive.
- Each element of words will contain between 1 and 50 characters, inclusive.
- Each element of words will consist of only uppercase letters ('A'-'Z').
 
Examples
0)

{"BAB", "ABA"}
Returns: "ABAB"
There are two strings of length 4 that contain both given words: "ABAB" and "BABA". "ABAB" comes earlier lexicographically.
1)

{"ABABA", "AKAKA", "AKABAS", "ABAKA"}
Returns: "ABABAKAKABAS"
2)

{"AAA","BBB", "CCC", "ABC", "BCA", "CAB"}
Returns: "AAABBBCABCCC"
3)

{"OFG", "SDOFGJTILM", "KBWNF", "YAAPO", "AWX", "VSEAWX", "DOFGJTIL", "YAA"}
Returns: "KBWNFSDOFGJTILMVSEAWXYAAPO"
4)

{"NVCSKFLNVS", "HUFSPMRI", "FLNV", "KMQD", "RPJK", "NVSQORP", "UFSPMR", "AIHUFSPMRI"}
Returns: "AIHUFSPMRINVCSKFLNVSQORPJKMQD"
5)

{"STRING", "RING"}
Returns: "STRING"
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.
*/

package srm302;

import static org.junit.Assert.*;
import org.junit.*;

public class JoinedStringTest {

  static void testJoin(String[] words, String expected) {
    String res = JoinedString.joinWords(words);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    String[] words = new String[] { "BAB", "ABA" };
    testJoin(words, "ABAB");
  }

  @Test
  public void ex1() {
    String[] words = new String[] { "ABABA", "AKAKA", "AKABAS", "ABAKA" };
    testJoin(words, "ABABAKAKABAS");
  }

  @Test
  public void ex2() {
    String[] words = new String[] { "AAA", "BBB", "CCC", "ABC", "BCA", "CAB" };
    testJoin(words, "AAABBBCABCCC");
  }

  @Test
  public void ex3() {
    String[] words = new String[] { "OFG", "SDOFGJTILM", "KBWNF", "YAAPO", "AWX", "VSEAWX", "DOFGJTIL", "YAA" };
    testJoin(words, "KBWNFSDOFGJTILMVSEAWXYAAPO");
  }

  @Test
  public void ex4() {
    String[] words = new String[] { "NVCSKFLNVS", "HUFSPMRI", "FLNV", "KMQD", "RPJK", "NVSQORP", "UFSPMR", "AIHUFSPMRI" };
    testJoin(words, "AIHUFSPMRINVCSKFLNVSQORPJKMQD");
  }

  @Test
  public void ex5() {
    String[] words = new String[] { "STRING", "RING" };
    testJoin(words, "STRING");
  }

}
