/*
Problem Statement

In the first half of the 20th century, around the time that Tagalog became the national language of the Philippines, a standardized alphabet was introduced to be used in Tagalog school books (since then, the national language has changed to a hybrid "Pilipino" language, as Tagalog is only one of several major languages spoken in the Philippines).

Tagalog's 20-letter alphabet is as follows:

a b k d e g h i l m n ng o p r s t u w y
Note that not all letters used in English are present, 'k' is the third letter, and 'ng' is a single letter that comes between 'n' and 'o'.

You are compiling a Tagalog dictionary, and for people to be able to find words in it, the words need to be sorted in alphabetical order with reference to the Tagalog alphabet. Given a list of Tagalog words as a String[], return the same list in Tagalog alphabetical order.

 
Definition

Class:  TagalogDictionary
Method: sortWords
Parameters: String[]
Returns:    String[]
Method signature: String[] sortWords(String[] words)
(be sure your method is public)
    
 
Notes
- Any 'n' followed followed by a 'g' should be considered a single 'ng' letter (the one that comes between 'n' and 'o').
 
Constraints
- words will contain between 1 and 50 elements, inclusive.
- Each element of words will contain between 1 and 50 characters, inclusive.
- Each character of each element of words will be a valid lowercase letter that appears in the Tagalog alphabet.
- Each element of words will be distinct.
 
Examples
0)

{"abakada","alpabet","tagalog","ako"}
Returns: {"abakada", "ako", "alpabet", "tagalog" }
The tagalog word for "alphabet", a Tagalogization of the English word "alphabet", the name of the language, and the word for "I".
1)

{"ang","ano","anim","alak","alam","alab"}
Returns: {"alab", "alak", "alam", "anim", "ano", "ang" }
A few "A" words that are alphabetically close together.
2)

{"siya","niya","kaniya","ikaw","ito","iyon"}
Returns: {"kaniya", "ikaw", "ito", "iyon", "niya", "siya" }
Common Tagalog pronouns.
3)

{"kaba","baka","naba","ngipin","nipin"}
Returns: {"baka", "kaba", "naba", "nipin", "ngipin" }
4)

{"knilngiggnngginggn","ingkigningg","kingkong","dingdong","dindong","dingdont","ingkblot"}
Returns: 
{"kingkong",
"knilngiggnngginggn",
"dindong",
"dingdont",
"dingdong",
"ingkblot",
"ingkigningg" }
5)

{"silangang", "baka", "bada", "silang"}
Returns: {"baka", "bada", "silang", "silangang" }
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.
*/

package srm342;

import static org.junit.Assert.*;
import org.junit.*;

public class TagalogDictionaryTest {

  static void testTagalogDict(String[] words, String[] expected) {
    String[] sorted = TagalogDictionary.sortWords(words);
    assertEquals(expected.length, sorted.length);
    for (int i = 0; i < sorted.length; i++) {
      assertEquals(expected[i], sorted[i]);
    }
  }

  @Test
  public void ex0() {
    String[] words = new String[] { "abakada", "alpabet", "tagalog", "ako" };
    String[] expected = new String[] { "abakada", "ako", "alpabet", "tagalog" };
    testTagalogDict(words, expected);
  }

  @Test
  public void ex1() {
    String[] words = new String[] { "ang", "ano", "anim", "alak", "alam", "alab" };
    String[] expected = new String[] { "alab", "alak", "alam", "anim", "ano", "ang" };
    testTagalogDict(words, expected);
  }

  @Test
  public void ex2() {
    String[] words = new String[] { "siya", "niya", "kaniya", "ikaw", "ito", "iyon" };
    String[] expected = new String[] { "kaniya", "ikaw", "ito", "iyon", "niya", "siya" };
    testTagalogDict(words, expected);
  }

  @Test
  public void ex3() {
    String[] words = new String[] { "kaba", "baka", "naba", "ngipin", "nipin" };
    String[] expected = new String[] { "baka", "kaba", "naba", "nipin", "ngipin" };
    testTagalogDict(words, expected);
  }

  @Test
  public void ex4() {
    String[] words = new String[] { "knilngiggnngginggn", "ingkigningg", "kingkong", "dingdong", "dindong", "dingdont", "ingkblot" };
    String[] expected = new String[] { "kingkong", "knilngiggnngginggn", "dindong", "dingdont", "dingdong", "ingkblot", "ingkigningg" };
    testTagalogDict(words, expected);
  }

  @Test
  public void ex5() {
    String[] words = new String[] { "silangang", "baka", "bada", "silang" };
    String[] expected = new String[] { "baka", "bada", "silang", "silangang" };
    testTagalogDict(words, expected);
  }

}
