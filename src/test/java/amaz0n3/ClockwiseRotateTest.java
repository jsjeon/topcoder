package amaz0n3;

import static org.junit.Assert.*;
import org.junit.*;

public class ClockwiseRotateTest {

  static void test(String input, String expected) {
    String res = ClockwiseRotate.rotate(input);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    String input = "2\n1 2\n3 4\n";
    String expected = "3 1\n4 2\n";
    test(input, expected);
  }

  @Test
  public void ex1() {
    String input ="2\n1 2 3\n4 5 6\n";
    String expected = "ERROR";
    test(input, expected);
  }

  @Test
  public void ex2() {
    String input = "1\n1\n";
    String expected = "1\n";
    test(input, expected);
  }

  @Test
  public void ex3() {
    String input = "3\n1 2 3\n4 5 6\n7 8 9\n";
    String expected ="4 1 2\n7 5 3\n8 9 6\n";
    test(input, expected);
  }

  @Test
  public void ex4() {
    String input = "4\n1 2 3 4\n5 6 7 8\n9 10 11 12\n13 14 15 16\n";
    String expected ="5 1 2 3\n9 10 6 4\n13 11 7 8\n14 15 16 12\n";
    test(input, expected);
  }

}
