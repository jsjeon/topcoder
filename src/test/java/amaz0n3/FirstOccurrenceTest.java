/*
Given a sorted array A of n elements possibly with duplicates, find the index of the first occurrence of a given number.

Eg:
Array arr = [1,2,2,2,4,5,5,6,6,6]

firstOccurrence(arr, 2) => 1
firstOccurrence(arr, 5) => 5

Array arr = [1,1,2,2,2,2,3]

firstOccurrence(arr, 2) => 2

Array arr = [1,2]

firstOccurrence(arr, 1) => 0
 */

package amaz0n3;

import static org.junit.Assert.*;
import org.junit.*;

public class FirstOccurrenceTest {

  static void test(int[] arr, int target, int expected) {
    int res = FirstOccurrence.firstOccurrence(arr, target);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    int[] arr = new int[] { 1, 2, 2, 2, 4, 5, 5, 6, 6, 6 };
    test(arr, 2, 1);
    test(arr, 5, 5);
  }

  @Test
  public void ex1() {
    int[] arr = new int[] { 1, 1, 2, 2, 2, 2, 3 };
    test(arr, 2, 2);
  }

  @Test
  public void ex2() {
    int[] arr = new int[] { 1, 2 };
    test(arr, 1, 0);
  }

}
