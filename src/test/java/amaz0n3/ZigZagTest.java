package amaz0n3;

import static org.junit.Assert.*;
import org.junit.*;

public class ZigZagTest {

  static void test(ZigZag.Node root, String expected) {
    String res = ZigZag.toString(root);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    ZigZag.Node n1 = new ZigZag.Node("1");

    ZigZag.Node n2 = new ZigZag.Node("2");
    ZigZag.Node n3 = new ZigZag.Node("3");
    n1.left = n2;
    n1.right = n3;

    ZigZag.Node n4 = new ZigZag.Node("4");
    ZigZag.Node n5 = new ZigZag.Node("5");
    n2.left = n4;
    n2.right = n5;

    ZigZag.Node n6 = new ZigZag.Node("6");
    ZigZag.Node n7 = new ZigZag.Node("7");
    n3.left = n6;
    n3.right = n7;

    test(n1, "1237654");
  }

}
