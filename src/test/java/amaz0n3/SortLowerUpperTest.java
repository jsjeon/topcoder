package amaz0n3;

import static org.junit.Assert.*;
import org.junit.*;

public class SortLowerUpperTest {

  static void test(String input, String expected) {
    String res = SortLowerUpper.sort(input);
    assertEquals(expected, res);
  }

  @Test
  public void ex0() {
    String input = "a cBd LkmY";
    String expected = "acdkm BLY";
    test(input, expected);
  }

}
