/*
Imagine a museum floor that looks like this: 

.#.G. 
..#.. 
G.... 
..#.. 

G == Museum Guard 
# == obstruction/impassable obstacle 
. == empty space 

Write a piece of code that will find the nearest guard for each open floor space. Diagonal moves are not allowed. The output should convey this information: 

2#1G1 
12#12 
G1223 
12#34 

You may choose how you want to receive the input and output. For example, you may use a 2-d array, as depicted here, or you may use a list of points with features, if you deem that easier to work with, as long as the same information is conveyed. 
*/

package go0g1e;

import java.util.Queue;
import java.util.ArrayDeque;

public class MuseumGuard {

  final static String nl = System.getProperty("line.separator");

  static int[][] initDistance(String[] floor) {
    int row = floor.length;
    int col = floor[0].length();
    int rc = row * col;
    int[][] distance = new int[row][col];

    for (int i = 0; i < row; i++) {
      char[] cs = floor[i].toCharArray();
      int j = 0;
      for (char c : cs) {
        int init = rc;
        if (c == 'G') init = 0;
        else if (c == '#') init = Integer.MAX_VALUE;
        distance[i][j++] = init;
      }
    }
    return distance;
  }

  static String[] display(String[] floor, int[][] distance) {
    int row = floor.length;
    int col = floor[0].length();

    StringBuilder buf = new StringBuilder();
    for (int i = 0; i < row; i++) {
      char[] cs = floor[i].toCharArray();
      int j = 0;
      for (char c : cs) {
        if (c == 'G' || c == '#') buf.append(c);
        else buf.append(distance[i][j]);
        j++;
      }
      buf.append(nl);
    }
    return buf.toString().split(nl);
  }

  // Floyd-Warshall
  // n[i][j] = 1 + min(n[i-1][j], n[i+1][j], n[i][j-1], n[i][j+1])
  public static String[] nearestGuardFW(String[] floor) {
    int row = floor.length;
    int col = floor[0].length();
    int rc = row * col;
    int[][] distance = initDistance(floor);

    boolean updated = true;
    while (updated) {
      updated = false;
      for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
          // either G or #
          if (distance[i][j] == 0 || distance[i][j] == Integer.MAX_VALUE) continue;

          int m = rc;
          if (i > 0)
            m = Math.min(m, distance[i-1][j]);
          if (i < row-1)
            m = Math.min(m, distance[i+1][j]);
          if (j > 0)
            m = Math.min(m, distance[i][j-1]);
          if (j < col-1)
            m = Math.min(m, distance[i][j+1]);

          if (m + 1 < distance[i][j]) {
            updated = true;
            distance[i][j] = m + 1;
          }
        }
      }
    }

    return display(floor, distance);
  }

  static class Pos {
    int x;
    int y;
    public Pos(int x, int y) {
      this.x = x;
      this.y = y;
    }
  }

  // BFS
  public static String[] nearestGuardBFS(String[] floor) {
    int row = floor.length;
    int col = floor[0].length();
    int[][] distance = initDistance(floor);

    Queue<Pos> q = new ArrayDeque<>();
    for (int i = 0; i < row; i++) {
      for (int j = 0; j < col; j++) {
        if (distance[i][j] == 0) {
          q.add(new Pos(i,j));
        }
      }
    }

    while (!q.isEmpty()) {
      Pos p = q.poll();
      int i = p.x;
      int j = p.y;
      int d = distance[i][j] + 1;
      if (i > 0 && distance[i-1][j] != Integer.MAX_VALUE) {
        int nd = Math.min(distance[i-1][j], d);
        if (nd < distance[i-1][j]) {
          distance[i-1][j] = nd;
          q.add(new Pos(i-1, j));
        }
      }
      if (i < row-1 && distance[i+1][j] != Integer.MAX_VALUE) {
        int nd = Math.min(distance[i+1][j], d);
        if (nd < distance[i+1][j]) {
          distance[i+1][j] = nd;
          q.add(new Pos(i+1, j));
        }
      }
      if (j > 0 && distance[i][j-1] != Integer.MAX_VALUE) {
        int nd = Math.min(distance[i][j-1], d);
        if (nd < distance[i][j-1]) {
          distance[i][j-1] = nd;
          q.add(new Pos(i, j-1));
        }
      }
      if (j < col-1 && distance[i][j+1] != Integer.MAX_VALUE) {
        int nd = Math.min(distance[i][j+1], d);
        if (nd < distance[i][j+1]) {
          distance[i][j+1] = nd;
          q.add(new Pos(i, j+1));
        }
      }
    }

    return display(floor, distance);
  }

}

