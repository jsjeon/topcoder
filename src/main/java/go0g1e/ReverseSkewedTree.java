package go0g1e;

public class ReverseSkewedTree {

  public static class Node {
    char c;
    Node left;
    Node right;

    public Node(char c) {
      this.c = c;
      left = null;
      right = null;
    }

    public void addChildren(char c1, char c2) {
      ReverseSkewedTree.Node n1 = new ReverseSkewedTree.Node(c1);
      ReverseSkewedTree.Node n2 = new ReverseSkewedTree.Node(c2);
      this.left = n1;
      this.right = n2;
    }

    @Override
    public String toString() {
      String lstr = left == null ? "" : left.toString();
      String rstr = right == null ? "" : right.toString();
      return lstr + c + rstr;
    }

    public boolean isSkewed() {
      if (left == null && right == null) return true;
      if (left == null || right == null) return false;
      return left.isSkewed() && (right.left == null && right.right == null);
    }
  }

  public static Node reverse(Node root) {
    Node nRoot = reverseHelper(root);
    root.left = null;
    root.right = null;
    return nRoot;
  }

  public static Node reverseHelper(Node n) {
    if (!n.isSkewed()) return null;

    if (n.left != null && n.right != null) {
      Node nRoot = reverseHelper(n.left);
      n.left.left = n.right;
      n.left.right = n;
      return nRoot;
    } else {
      return n;
    }
  }

}

