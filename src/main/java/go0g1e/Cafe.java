package go0g1e;

public class Cafe {

  int enjoyment;
  int penalty;

  public Cafe(int e, int p) {
    enjoyment = e;
    penalty = p;
  }

  public static int maxEnjoymentRev(Cafe[] cafes) {
    return maxEnjoymentRevHelper(cafes, 0);
  }

  static int maxEnjoymentRevHelper(Cafe[] cafes, int idx) {
    if (idx >= cafes.length) return 0;
    // maybe, memoization here, like maxSofar[idx]
    int choose_here = cafes[idx].enjoyment +
        maxEnjoymentRevHelper(cafes, idx + cafes[idx].penalty + 1);
    int not_choose_here = maxEnjoymentRevHelper(cafes, idx + 1);
    return Math.max(choose_here, not_choose_here);
  }

  public static int maxEnjoymentDP(Cafe[] cafes) {
    int len = cafes.length;
    int[] maxE = new int [len];
    for (int i = 0; i < len; i++) {
      int maxTemp = 0;
      for (int j = 0; j < i; j++) {
        if (j + cafes[j].penalty < i && maxTemp < maxE[j]) {
          maxTemp = maxE[j];
        }
      }
      maxE[i] = maxTemp + cafes[i].enjoyment;
    }
    int fMax = 0;
    for (int i = 0; i < len; i++) {
      if (maxE[i] > fMax) {
        fMax = maxE[i];
      }
    }
    return fMax;
  }

}

