/*
The "surpasser" of an element in an array is defined as the number of elements that are to the "right" and bigger than itself. 

Example: 
Array: 
[2, 7, 5, 5, 2, 7, 0, 8, 1] 
The "surpassers" are 
[5, 1, 2, 2, 2, 1, 2, 0, 0] 

Question: Find the maximum surpasser of the array. 

In this example, maximum surpasser = 5
*/

package go0g1e;

//import java.util.Arrays;

public class Surpasser {

  /* NOTE: this is a variant of counting inversion problem
   * key idea: when merging sub array, say l[...] and r[...]
   * if l_i < r_j and l_i is chosen to be copied to the merged array,
   * it is guaranteed that r_k s.t. j <= k is also bigger than l_i
   * so, l_i's surpasser count at that moment is 'right' - j + 1 (inclusive)
   * we can accumulate such counts and while doing merge sort
   */

  static void mergeSort(int[] arr, int[] sur, int[] buf, int[] surBuf,
                        int left, int right)
  {
    if (left >= right) return;

    int mid = left + (right - left) / 2;
    mergeSort(arr, sur, buf, surBuf, left, mid);
    mergeSort(arr, sur, buf, surBuf, mid+1, right);

    int i = left;
    int j = mid+1;
    int k = left;
    // merging
    while (i <= mid && j <= right) {
      if (arr[i] < arr[j]) { // surpass!
        buf[k] = arr[i];
        surBuf[k] = sur[i] + (right - j + 1);
        k++; i++;
      } else { // arr[i] >= arr[j]
        buf[k] = arr[j];
        surBuf[k] = sur[j];
        k++; j++;
      }
    }
    // remaining left sub (if any)
    while (i <= mid) {
      buf[k] = arr[i];
      surBuf[k] = sur[i];
      k++; i++;
    }
    // remaining right sub (if any)
    while (j <= right) {
      buf[k] = arr[j];
      surBuf[k] = sur[j];
      k++; j++;
    }
    // copy back to originals
    for (i = left; i <= right; i++) {
      arr[i] = buf[i];
      sur[i] = surBuf[i];
    }
    //System.out.println(Arrays.toString(sur));
    //System.out.println(Arrays.toString(arr));
  }

  static int[] mergeSortSur(int[] arr) {
    int len = arr.length;
    int[] sur = new int[len];
    for (int i = 0; i < len; i++) {
      sur[i] = 0;
    }
    int[] buf = new int[len];
    int[] surBuf = new int[len];
    mergeSort(arr, sur, buf, surBuf, 0, len-1);
    return sur;
  }

  public static int max(int[] arr) {
    int[] sur = mergeSortSur(arr);
    int maxSur = 0;
    for (int i = 0; i < sur.length; i++) {
      maxSur = Math.max(maxSur, sur[i]);
    }
    return maxSur;
  }

}

