package go0g1e;

public class Increment {

  // [9, 9] => true
  static boolean needsExt(int[] arr) {
    boolean res = true;
    for (int i = 0; i < arr.length; i++) {
      if (arr[i] != 9) {
        res = false;
        break;
      }
    }
    return res;
  }

  // [1, 2, 3] => [1, 2, 4]
  // [3, 6, 9] => [3, 7, 0]
  // [9, 9] => [1, 0, 0]
  public static int[] inc(int[] arr) {
    if (needsExt(arr)) {
      int[] res = new int[arr.length + 1];
      res[0] = 1;
      for (int i = 0; i < arr.length; i++) {
        res[i+1] = 0;
      }
      return res;
    }

    int len = arr.length;
    int[] res = new int[len];
    System.arraycopy(arr, 0, res, 0, len);

    boolean propagation = true;
    int i = len-1;
    while (propagation) {
      res[i]++;
      if (res[i] > 9) {
        res[i--] = 0;
      } else {
        propagation = false;
      }
    }
    return res;
  }

  public static int toInt(int[] arr) {
    int acc = 0;
    for (int i = 0; i < arr.length; i++) {
      acc *= 10;
      acc += arr[i];
    }
    return acc;
  }

}
