package go0g1e;

public class Quote {

  static char counterPart(char c) {
    if (c == '\'') return '"';
    if (c == '"') return '\'';
    return c;
  }

  public static String subst(String txt) {
    StringBuilder buf = new StringBuilder();
    int idx = 0;
    int l = -1;
    for (char c : txt.toCharArray()) {
      if (l >= 0) {
        if (idx <= l) {
          idx++;
          continue;
        } else {
          l = -1;
        }
      }
      if (c == '\'' || c == '"') {
        l = txt.lastIndexOf(c);
        char cc = counterPart(c);
        buf.append(cc);
        buf.append(txt.substring(idx+1, l));
        buf.append(cc);
      } else {
        buf.append(c);
      }
      idx++;
    }
    return buf.toString();
  }

}
