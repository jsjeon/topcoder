package faceb00k;

public class Matcher {

  public static boolean isMatch(String pattern, String target) {
    if (pattern.length() == 0) {
      return target.length() == 0;
    }

    char p = pattern.charAt(0);
    String rP = pattern.substring(1);

    if (target.length() == 0) {
      if (p != '*') return false;
      return isMatch(rP, target);
    }

    switch (p) {
      case '?':
        return isMatch(rP, target.substring(1));
      case '*':
        for (int i = 0; i <= target.length(); i++) {
          boolean sub = isMatch(rP, target.substring(i));
          if (sub) return sub;
        }
        return false;
      default:
        char t = target.charAt(0);
        if (t != p) return false;
        return isMatch(rP, target.substring(1));
    }
  }

}

