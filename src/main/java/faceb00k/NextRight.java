package faceb00k;

/* 
For each node in a binary tree find the next right node on the same depth. Write a function that takes root node and populates "next" with the answer for each node. 

A 
/ \ 
B -> C 
/ / \ 
D -> F-> G 
/ \ 
H -> I 

class Node { 
Node left; 
Node right; 
Node next; // <-- answer should be stored here 
}; 

B.next = C 
D.next = F 
F.next = G 
H.next = I 
{A, C, G, I}.next = null 
*/

import java.util.*;

public class NextRight {

  static class Node {
    Node left;
    Node right;
    Node next;

    public Node() {
      left = null;
      right = null;
      next = null;
    }
  }

  public static void populateNext(Node root) {
    List<Node> q1 = new ArrayList<>();
    List<Node> q2 = new ArrayList<>();

    q1.add(root);
    while (!q1.isEmpty()) {
      Node cur = q1.remove(0);
      if (cur.left != null) q2.add(cur.left);
      if (cur.right != null) q2.add(cur.right);

      // when depth is changed, populate next
      if (q1.isEmpty()) {
        Node x = null;
        Node y = null;
        for (Node n : q2) {
          q1.add(n);
          y = n;
          if (x == null) {
            // 1st elt in the next depth
            x = n;
            continue;
          } else {
            // link the next node
            x.next = y;
            // and move to that one
            x = y;
          }
        }

        q2.clear();
      }
    }
  }

}
