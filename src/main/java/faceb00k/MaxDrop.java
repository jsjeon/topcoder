package faceb00k;

/*
find the maximum drop: a[i] - a[j], where i < j
 */

public class MaxDrop {

  public static int find(int[] arr) {
    if (arr.length <= 1) return -1;

    int len = arr.length;
    int maxSofar = arr[0];
    int maxDropSofar = -1;
    for (int i = 1; i < len; i++) {
      if (arr[i] > maxSofar) {
        maxSofar = arr[i];
      } else {
        int diff = maxSofar - arr[i];
        if (diff > maxDropSofar) {
          maxDropSofar = diff;
        }
      }
    }
    return maxDropSofar;
  }

}
