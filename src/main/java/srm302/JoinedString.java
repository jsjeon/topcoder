/*
Problem Statement
    You are given a String[] words. Return the shortest String that contains all the words as substrings. If there are several possible answers, return the one that comes first lexicographically.
 
Definition

Class:  JoinedString
Method: joinWords
Parameters: String[]
Returns:    String
Method signature: String joinWords(String[] words)
(be sure your method is public)

 
Constraints
- words will contain between 1 and 12 elements, inclusive.
- Each element of words will contain between 1 and 50 characters, inclusive.
- Each element of words will consist of only uppercase letters ('A'-'Z').
 
Examples
0)

{"BAB", "ABA"}
Returns: "ABAB"
There are two strings of length 4 that contain both given words: "ABAB" and "BABA". "ABAB" comes earlier lexicographically.
1)

{"ABABA", "AKAKA", "AKABAS", "ABAKA"}
Returns: "ABABAKAKABAS"
2)

{"AAA","BBB", "CCC", "ABC", "BCA", "CAB"}
Returns: "AAABBBCABCCC"
3)

{"OFG", "SDOFGJTILM", "KBWNF", "YAAPO", "AWX", "VSEAWX", "DOFGJTIL", "YAA"}
Returns: "KBWNFSDOFGJTILMVSEAWXYAAPO"
4)

{"NVCSKFLNVS", "HUFSPMRI", "FLNV", "KMQD", "RPJK", "NVSQORP", "UFSPMR", "AIHUFSPMRI"}
Returns: "AIHUFSPMRINVCSKFLNVSQORPJKMQD"
5)

{"STRING", "RING"}
Returns: "STRING"
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.
*/

package srm302;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Collections;

public class JoinedString {

  // edge ::= src --cnt--> dest
  static class Edge {
    int subCnt;
    Node dest;

    public Edge(int cnt, Node dest) {
      subCnt = cnt;
      this.dest = dest;
    }

    @Override
    public String toString() {
      return "--" + subCnt + "--> " + dest.str;
    }
  }

  // node ::= str, edges
  static class Node {
    String str;
    List<Edge> edges;

    public Node(String str) {
      this.str = str;
      edges = new ArrayList<>();
    }

    public void add(Edge e) {
      edges.add(e);
    }

    @Override
    public String toString() {
      StringBuilder buf = new StringBuilder();
      buf.append("[" + this.str + "]" + System.lineSeparator());
      for (Edge e : edges) {
        buf.append("  " + e.toString() + System.lineSeparator());
      }
      return buf.toString();
    }
  }

  Set<Node> nodes;

  protected JoinedString() {
    nodes = new HashSet<>();
  }

  protected void addWord(String word) {
    // if it is fully covered by another word, ignore it
    // to do this, longer words should be added ahead
    for (Node n : nodes) {
      if (n.str.contains(word)) return;
    }

    Node nn = new Node(word);
    int nn_len = word.length();
    for (Node n : nodes) {
      int n_len = n.str.length();

      //  n: ...common
      // nn:    common...
      String maxCommon = "";
      for (int i = 1; i <= n_len && i <= nn_len; i++) {
        String n_suffix = n.str.substring(n_len-i, n_len);
        String nn_prefix = word.substring(0, i);
        if (n_suffix.equals(nn_prefix)) maxCommon = n_suffix;
      }
      if (!maxCommon.isEmpty()) {
        Edge e = new Edge(maxCommon.length(), nn);
        n.add(e);
      }

      // nn: ...common
      //  n:    common...
      maxCommon = "";
      for (int i = 1; i <= n_len && i <= nn_len; i++) {
        String nn_suffix = word.substring(nn_len-i, nn_len);
        String n_prefix = n.str.substring(0, i);
        if (n_prefix.equals(nn_suffix)) maxCommon = n_prefix;
      }
      if (!maxCommon.isEmpty()) {
        Edge ee = new Edge(maxCommon.length(), n);
        nn.add(ee);
      }
    }
    nodes.add(nn);
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder();
    for (Node n : nodes) {
      buf.append(n.toString() + System.lineSeparator());
    }
    return buf.toString();
  }

  /*
  goal: find a path that visits all nodes and maximizes edge weights

  [AKABAS]

  [ABABA]
    --1--> AKAKA
    --1--> AKABAS
    --3--> ABAKA

  [AKAKA]
    --1--> ABABA
    --3--> AKABAS
    --1--> ABAKA

  [ABAKA]
    --3--> AKABAS
    --1--> ABABA
    --3--> AKAKA

  path: [ABABA] --3--> [ABAKA] --3--> [AKAKA] --3--> [AKABAS]
  result: ABABAKAKABAS
  */
  protected String join() {
    return dangledVisit("", new HashSet<Node>(), new HashSet<Node>(nodes));
  }

  static String updateShortest(String prev, String next) {
    if (prev == null) return next;
    if (next.length() < prev.length()) return next;
    if (next.length() == prev.length()) {
      if (next.compareTo(prev) < 0) return next;
    }
    return prev;
  }

  String dangledVisit(String acc, Set<Node> visited, Set<Node> remaining) {
    String shortest = null;
    for (Node n : remaining) {
      Set<Node> visited_c = new HashSet<>(visited);
      Set<Node> remaining_c = new HashSet<>(remaining);
      visited_c.add(n);
      remaining_c.remove(n);
      String acc_c = visit(n.str, n, visited_c, remaining_c);
      String temp;
      if (acc.compareTo(acc_c) < 0) {
        temp = acc + acc_c;
      } else {
        temp = acc_c + acc;
      }
      shortest = updateShortest(shortest, temp);
    }
    return shortest;
  }

  String visit(String acc, Node n, Set<Node> visited, Set<Node> remaining) {
    // base case: no more node to be visited
    if (remaining.isEmpty()) return acc;

    // if this node is not connected
    if (n.edges.isEmpty()) {
      return dangledVisit(acc, visited, remaining);
    }
    // o.w., visit the largest weight edge(s)
    else {
      int maxWeight = -1;
      List<Node> nns = new ArrayList<>();
      for (Edge e : n.edges) {
        // already visited, pass
        if (visited.contains(e.dest)) continue;
        // largest weight
        if (e.subCnt > maxWeight) {
          maxWeight = e.subCnt;
          nns.clear();
          nns.add(e.dest);
        }
        // tie
        else if (e.subCnt == maxWeight) {
          nns.add(e.dest);
        }
      }
      // if every node is already visited
      if (nns.isEmpty()) {
        return dangledVisit(acc, visited, remaining);
      } else {
        String shortest = null;
        for (Node nn : nns) {
          Set<Node> visited_c = new HashSet<>(visited);
          Set<Node> remaining_c = new HashSet<>(remaining);
          visited_c.add(nn);
          remaining_c.remove(nn);
          String next_acc = acc + nn.str.substring(maxWeight, nn.str.length());
          String temp = visit(next_acc, nn, visited_c, remaining_c);
          shortest = updateShortest(shortest, temp);
        }
        return shortest;
      }
    }
  }

  // longer one will appear ahead
  static class StringLengthComparator implements Comparator<String> {
    public StringLengthComparator() {
    }

    public int compare(String s1, String s2) {
      return s2.length() - s1.length();
    }
  }

  public static String joinWords(String[] words) {
    JoinedString jstr = new JoinedString();
    List<String> wordList = Arrays.asList(words);
    Collections.sort(wordList, new StringLengthComparator());
    for (String word : wordList) {
      jstr.addWord(word);
    }
    return jstr.join();
  }

}
