/*
Problem Statement
        There are several cities in the country, and some of them are connected by bidirectional roads. Unfortunately, some of the roads are damaged and cannot be used right now. Your goal is to rebuild enough of the damaged roads that there is a functional path between every pair of cities.
You are given String[] roads, each element of which describes a single road. Damaged roads are formatted as "id city1 city2 cost" and non-damaged roads are formatted as "id city1 city2" (all quotes for clarity). In this notation, id is the unique identifier of the road, and city1 and city2 are the case-sensitive names of the two cities directly connected by that road. If the road is damaged, cost represents the price of rebuilding that road. Each id will be formatted "Cx" (quotes for clarity), where C is an uppercase letter and x is a digit. Every city in the country will appear at least once in roads.
Return a String containing a single space separated list of the identifiers of the roads that must be rebuilt to achieve your goal. If there are multiple possibilities, select the one with the minimal total reconstruction cost. If a tie still exists, return the String that comes first lexicographically. If it is impossible to achieve your goal, return "IMPOSSIBLE" (quotes for clarity only).
 
Definition
        
Class:    RoadReconstruction
Method:    selectReconstruction
Parameters:    String[]
Returns:    String
Method signature:    String selectReconstruction(String[] roads)
(be sure your method is public)
    
 
Notes
-    There can be more than one road between a pair of cities.
 
Constraints
-    roads will contain between 1 and 50 elements, inclusive.
-    Each element of roads will contain between 6 and 50 characters, inclusive.
-    Each element of roads will be formatted as "id city1 city2" or "id city1 city2 cost" (all quotes for clarity).
-    Each id will be formatted as "Cx" (quotes for clarity), where C is an uppercase letter ('A'-'Z') and x is a digit ('0'-'9').
-    Each id in roads will be distinct.
-    Each city1 and city2 will contain between 1 and 45 letters ('a'-'z', 'A'-'Z'), inclusive.
-    In each element of roads, city1 and city2 will be distinct.
-    Each cost will be an integer between 1 and 1000, inclusive, with no leading zeroes.
 
Examples
0)    
        
{"M1 Moscow Kiev 1", "M2 Minsk Kiev", "M3 Minsk Warsaw"}
Returns: "M1"
Rebuilding road M1 will make all three cities connected to each other.
1)    
        
{"R1 NY Washington", "M1 Moscow StPetersburg 1000"}
Returns: "IMPOSSIBLE"
Even after reconstuction of the road M1, the resulting road network won't be connected. So, the answer is "IMPOSSIBLE".
2)    
        
{"B1 Bratislava Havka"}
Returns: ""
3)    
        
{"M1 Moscow StPetersburg 1", "M2 Moscow Saratov 2", "S0 Saratov StPetersburg"}}
Returns: "M1"
4)    
        
{"O1 Beetown Fearnot 6","N7 Fearnot Hornytown","M8 Hornytown Belcher 10",
 "L5 Belcher Fearnot 8","C7 Fearnot Beetown 4","K7 Quiggleville Beetown 12",
 "H4 Beetown DryFork 6","Z0 Hornytown Belcher 1","O5 Belcher Quiggleville 10",
 "U7 Quiggleville Fearnot 2","A8 Fearnot Quiggleville 8","T6 Beetown DryFork 17",
 "E8 Quiggleville DryFork 8","Y4 DryFork Quiggleville 4","Q8 Hornytown DryFork 2",
 "J9 Quiggleville DryFork 19","M4 DryFork Quiggleville 7","T1 DryFork Fearnot 9",
 "G4 Fearnot DryFork 6","V9 Hornytown Beetown 5","O6 Quiggleville Beetown 4",
 "L8 Beetown Roachtown 5","D5 Belcher DryFork 8","W5 Belcher DryFork 1"}
Returns: "C7 L8 U7 W5 Z0"
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.
 */

package srm356;

import java.util.Map;
import java.util.HashMap;

import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;

import java.util.Comparator;

public class RoadReconstruction {

  protected final static String IM = "IMPOSSIBLE";

  static class UnionFindNode {
    int rank;
    String city;
    UnionFindNode parent;

    public UnionFindNode(String city) {
      this.city = city;
      rank = 0;
      parent = this;
    }

    public static void union(UnionFindNode n1, UnionFindNode n2) {
      UnionFindNode p1 = n1.find();
      UnionFindNode p2 = n2.find();
      if (p1 == p2) return;

      if (p1.rank > p2.rank) {
        p2.parent = p1;
      } else if (p1.rank < p2.rank) {
        p1.parent = p2;
      }
      else {
        p2.parent = p1;
        p1.rank++;
      }
    }

    public UnionFindNode find() {
      if (parent == this) return this;
      parent = parent.find();
      return parent;
    }

    public static boolean connected(UnionFindNode n1, UnionFindNode n2) {
      UnionFindNode p1 = n1.find();
      UnionFindNode p2 = n2.find();
      return p1 == p2;
    }
  }

  static class Road {
    String id;
    String src;
    String dest;
    int cost;

    public Road(String id, String src, String dest) {
      this.id = id;
      this.src = src;
      this.dest = dest;
      cost = 0;
    }

    public Road(String id, String src, String dest, int cost) {
      this(id, src, dest);
      this.cost = cost;
    }

    @Override
    public String toString() {
      if (cost == 0)
        return id + " " + src + " " + dest;
      else
        return id + " " + src + " " + dest + " " + cost;
    }
  }

  static class RoadCostComparator implements Comparator<Road> {
    public RoadCostComparator() {
    }
    public int compare(Road r1, Road r2) {
      int c = r1.cost - r2.cost;
      if (c != 0) return c;
      return r1.id.compareTo(r2.id);
    }
  }

  static class RoadIdComparator implements Comparator<Road> {
    public RoadIdComparator() {
    }
    public int compare(Road r1, Road r2) {
      return r1.id.compareTo(r2.id);
    }
  }

  Map<String, UnionFindNode> cities;
  Set<Road> roads;

  public RoadReconstruction() {
    cities = new HashMap<>();
    roads = new TreeSet<>(new RoadCostComparator());
  }

  public void addRoad(String road) {
    String[] items = road.split(" ");
    Road r = null;
    if (items.length == 4) {
      r = new Road(items[0], items[1], items[2], Integer.parseInt(items[3]));
    } else if (items.length == 3) {
      r = new Road(items[0], items[1], items[2]);
    }
    if (r == null) return;
    roads.add(r);
  }

  UnionFindNode findOrAddCity(String city) {
    if (cities.containsKey(city)) return cities.get(city);
    UnionFindNode n = new UnionFindNode(city);
    cities.put(city, n);
    return n;
  }

  boolean allConnected() {
    Set<UnionFindNode> root = new HashSet<>();
    for (Map.Entry<String, UnionFindNode> entry : cities.entrySet()) {
      UnionFindNode n = entry.getValue();
      root.add(n.find());
    }
    return root.size() == 1;
  }

  public String reconstruct() {
    Set<Road> reRoads = new TreeSet<>(new RoadIdComparator());
    // guaranteed to visit minimum cost roads first
    for (Road r : roads) {
      UnionFindNode city1 = findOrAddCity(r.src);
      UnionFindNode city2 = findOrAddCity(r.dest);
      if (!UnionFindNode.connected(city1, city2)) {
        UnionFindNode.union(city1, city2);
        if (r.cost != 0) reRoads.add(r);
      }
    }

    if (!allConnected()) return IM;

    if (reRoads.isEmpty()) return "";

    StringBuilder buf = new StringBuilder();
    for (Road r : reRoads) {
      buf.append(" " + r.id);
    }
    String ans = buf.toString();
    return ans.substring(1, ans.length());
  }

  public static String selectReconstruction(String[] roads) {
    RoadReconstruction rr = new RoadReconstruction();
    for (String road : roads) {
      rr.addRoad(road);
    }
    return rr.reconstruct();
  }

}

