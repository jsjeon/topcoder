/*
Problem Statement
    We can think of a cyclic word as a word written in a circle. To represent a cyclic word, we choose an arbitrary starting position and read the characters in clockwise order. So, "picture" and "turepic" are representations for the same cyclic word.

You are given a String[] words, each element of which is a representation of a cyclic word. Return the number of different cyclic words that are represented.

 
Definition

Class:  CyclicWords
Method: differentCW
Parameters: String[]
Returns:    int
Method signature: int differentCW(String[] words)
(be sure your method is public)
    
 
Constraints
- words will contain between 1 and 50 elements, inclusive.
- Each element of words will contain between 1 and 50 lowercase letters ('a'-'z'), inclusive.
 
Examples
0)

{ "picture", "turepic", "icturep", "word", "ordw" }
Returns: 2
"picture", "turepic" and "iceturep" are representations of the same cyclic word. "word" and "ordw" are representations of a second cyclic word.
1)

{ "ast", "ats", "tas", "tsa", "sat", "sta", "ttt" }
Returns: 3
2)

{"aaaa", "aaa", "aa", "aaaa", "aaaaa"}
Returns: 4
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.
*/

package srm358;

import java.util.HashSet;
import java.util.Set;

public class CyclicWords {

  public static String cycle(String word, int n) {
    int nn = n % word.length();
    return word.substring(nn, word.length()) + word.substring(0, nn);
  }

  /** returns the canonical cyclic word
   * define the "canonical" word as lexicographically first word among all cyclic words
   * e.g., picture, turepic, ... = cturepi
   */
  public static String cyclicCanonical(String word) {
    char maxChar = 'z';
    int index = word.length();
    for (int i = 0; i < word.length(); i++) {
      char c = word.charAt(i);
      if (c < maxChar) {
        maxChar = c;
        index = i;
      }
    }
    return cycle(word, index);
  }

  public static int differentCW(String[] words) {
    Set<String> cw = new HashSet<>();
    for (String word : words) {
      String canonical = cyclicCanonical(word);
      //System.out.println(word + " => " + canonical + System.lineSeparator());
      cw.add(canonical);
    }
    return cw.size();
  }

}
