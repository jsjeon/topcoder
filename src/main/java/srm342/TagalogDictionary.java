/*
Problem Statement

In the first half of the 20th century, around the time that Tagalog became the national language of the Philippines, a standardized alphabet was introduced to be used in Tagalog school books (since then, the national language has changed to a hybrid "Pilipino" language, as Tagalog is only one of several major languages spoken in the Philippines).

Tagalog's 20-letter alphabet is as follows:

a b k d e g h i l m n ng o p r s t u w y
Note that not all letters used in English are present, 'k' is the third letter, and 'ng' is a single letter that comes between 'n' and 'o'.

You are compiling a Tagalog dictionary, and for people to be able to find words in it, the words need to be sorted in alphabetical order with reference to the Tagalog alphabet. Given a list of Tagalog words as a String[], return the same list in Tagalog alphabetical order.

 
Definition

Class:  TagalogDictionary
Method: sortWords
Parameters: String[]
Returns:    String[]
Method signature: String[] sortWords(String[] words)
(be sure your method is public)
    
 
Notes
- Any 'n' followed followed by a 'g' should be considered a single 'ng' letter (the one that comes between 'n' and 'o').
 
Constraints
- words will contain between 1 and 50 elements, inclusive.
- Each element of words will contain between 1 and 50 characters, inclusive.
- Each character of each element of words will be a valid lowercase letter that appears in the Tagalog alphabet.
- Each element of words will be distinct.
 
Examples
0)

{"abakada","alpabet","tagalog","ako"}
Returns: {"abakada", "ako", "alpabet", "tagalog" }
The tagalog word for "alphabet", a Tagalogization of the English word "alphabet", the name of the language, and the word for "I".
1)

{"ang","ano","anim","alak","alam","alab"}
Returns: {"alab", "alak", "alam", "anim", "ano", "ang" }
A few "A" words that are alphabetically close together.
2)

{"siya","niya","kaniya","ikaw","ito","iyon"}
Returns: {"kaniya", "ikaw", "ito", "iyon", "niya", "siya" }
Common Tagalog pronouns.
3)

{"kaba","baka","naba","ngipin","nipin"}
Returns: {"baka", "kaba", "naba", "nipin", "ngipin" }
4)

{"knilngiggnngginggn","ingkigningg","kingkong","dingdong","dindong","dingdont","ingkblot"}
Returns: 
{"kingkong",
"knilngiggnngginggn",
"dindong",
"dingdont",
"dingdong",
"ingkblot",
"ingkigningg" }
5)

{"silangang", "baka", "bada", "silang"}
Returns: {"baka", "bada", "silang", "silangang" }
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.
*/

package srm342;

import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import java.util.Comparator;
import java.util.Collections;

public class TagalogDictionary {

  static final String[] _alphabet = {
    "a", "b", "k", "d", "e", "g", "h", "i", "l", "m",
    "n", "ng","o", "p", "r", "s", "t", "u", "w", "y"
  };

  // "a" -> 0, "b" -> 1, "k" -> 2, ...
  static final Map<String, Integer> _index;

  static {
    Map<String, Integer> aMap = new HashMap<>();
    for (int i = 0; i < _alphabet.length; i++) {
      aMap.put(_alphabet[i], i);
    }
    _index = Collections.unmodifiableMap(aMap);
  }

  static class DictNode {
    String alphabet;
    boolean endOfWord;
    // will be sorted in an alphabetic order by NodeComparator
    Set<DictNode> children;

    public DictNode() {
      alphabet = "";
      endOfWord = false;
      children = new TreeSet<>(new NodeComparator());
    }

    public DictNode(String alphabet) {
      this();
      this.alphabet = alphabet;
    }
  }

  static class NodeComparator implements Comparator<DictNode> {
    public NodeComparator() {
    }

    public int compare(DictNode n1, DictNode n2) {
      int idx1 = -1;
      int idx2 = -1;
      if (_index.containsKey(n1.alphabet)) idx1 = _index.get(n1.alphabet);
      if (_index.containsKey(n2.alphabet)) idx2 = _index.get(n2.alphabet);
      return idx1 - idx2;
    }
  }

  DictNode root;

  public TagalogDictionary() {
    root = new DictNode();
  }

  public void addWord(String word) {
    DictNode iter = root;

    String remaining = word;
    while (remaining.length() > 0) {
      int len = remaining.length();
      // for each alphabet
      String alphabet = "";
      if (remaining.startsWith("ng")) {
        alphabet = "ng";
        remaining = remaining.substring(2, len);
      } else {
        alphabet = remaining.substring(0, 1);
        remaining = remaining.substring(1, len);
      }

      DictNode next = null;
      // move down to the corresponding node if exists
      for (DictNode child : iter.children) {
        if (child.alphabet.equals(alphabet)) {
          next = child;
          break;
        }
      }
      // o.w., make a new node for the current alphabet
      if (next == null) {
        next = new DictNode(alphabet);
        iter.children.add(next);
      }
      iter = next;
    }
    iter.endOfWord = true;
  }

  public String[] toArray() {
    List<String> words = new ArrayList<>();
    visit(root, "", words);
    return words.toArray(new String[0]);
  }

  void visit(DictNode node, String acc, List<String> words) {
    String acc_p = acc + node.alphabet;
    // leaf
    if (node.children.isEmpty()) {
      words.add(acc_p);
    }
    else {
      if (node.endOfWord) {
        words.add(acc_p);
      }
      // visit children recursively
      for (DictNode child : node.children) {
        visit(child, acc_p, words);
      }
    }
  }

  public static String[] sortWords(String[] words) {
    TagalogDictionary dict = new TagalogDictionary();
    for (String word : words) {
      dict.addWord(word);
    }
    return dict.toArray();
  }
}

