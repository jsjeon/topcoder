/*
Problem Statement
    	
You have some cards, each containing a positive integer. You are given a int[] d. Each element of d is one of those integers. The integers are not necessarily distinct.

You are also given an int goodValue. You are interested in non-empty subsets of cards with the following property: The product of integers written on those cards is exactly equal to goodValue.

Let X be the number of subsets with the above property. Compute and return the value (X modulo 1,000,000,007).

 
Definition
    	
Class:	GoodSubset
Method:	numberOfSubsets
Parameters:	int, int[]
Returns:	int
Method signature:	int numberOfSubsets(int goodValue, int[] d)
(be sure your method is public)
    
 
Constraints
-	goodValue will be between 1 and 2,000,000,000, inclusive.
-	d will contain between 1 and 100 elements, inclusive.
-	Each element of d will be between 1 and 2,000,000,000, inclusive.
 
Examples
0)	
    	
10
{2,3,4,5}
Returns: 1
There is only one good subset:{2,5}.
1)	
    	
6
{2,3,4,5,6}
Returns: 2
There are two good subsets: {2,3} and {6}.
2)	
    	
1
{1,1,1}
Returns: 7
All non-empty subsets of this set of cards are good.
3)	
    	
12
{1,2,3,4,5,6,7,8,9,10,11,12}
Returns: 6
4)	
    	
5
{1,2,3,4}
Returns: 0
*/

package srm632;

import java.util.Arrays;

public class GoodSubset {

  public static int numberOfSubsets(int goodValue, int[] d) {
    Arrays.sort(d);
    // now we can assume d is orderd
    return nOfS(goodValue, d, true);
  }

  static int nOfS(int v, int[] d, boolean isEmpty) {
    int len = d.length;
    if (len == 0) {
      return (!isEmpty && v == 1) ? 1 : 0;
    }

    // pick the largest one
    // recursion: choose that or not
    int largest = d[len-1];
    int[] sub = new int[len-1];
    System.arraycopy(d, 0, sub, 0, len-1);
    // avoid this element and keep searching the remaining
    if (largest > v || v % largest != 0) {
      return nOfS(v, sub, isEmpty);
    }
    // or, try both cases: add this or not
    else {
      return nOfS(v, sub, isEmpty) + nOfS(v / largest, sub, false);
    }
  }

}
