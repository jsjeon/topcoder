/*
Problem Statement
        
One day, Jamie noticed that many English words only use the letters A and B. Examples of such words include "AB" (short for abdominal), "BAA" (the noise a sheep makes), "AA" (a type of lava), and "ABBA" (a Swedish pop sensation).



Inspired by this observation, Jamie created a simple game. You are given two Strings: initial and target. The goal of the game is to find a sequence of valid moves that will change initial into target. There are two types of valid moves:

Add the letter A to the end of the string.
Add the letter B to the end of the string and then reverse the entire string. (After the reversal the newly-added B becomes the first character of the string).
Return "Possible" (quotes for clarity) if there is a sequence of valid moves that will change initial into target. Otherwise, return "Impossible".

 
Definition
        
Class:    ABBADiv1
Method:    canObtain
Parameters:    String, String
Returns:    String
Method signature:    String canObtain(String initial, String target)
(be sure your method is public)
    
 
Constraints
-    The length of initial will be between 1 and 49, inclusive.
-    The length of target will be between 2 and 50, inclusive.
-    target will be longer than initial.
-    Each character in initial and each character in target will be either 'A' or 'B'.
 
Examples
0)    
        
"A"
"BABA"
Returns: "Possible"
Jamie can perform the following moves:
Initially, the string is "A".
Jamie adds a 'B' to the end of the string and then reverses the string. Now the string is "BA".
Jamie adds a 'B' to the end of the string and then reverses the string. Now the string is "BAB".
Jamie adds an 'A' to the end of the string. Now the string is "BABA".
Since there is a sequence of moves which starts with "A" and creates the string "BABA", the answer is "Possible".
1)    
        
"BAAAAABAA"
"BAABAAAAAB"
Returns: "Possible"
Jamie can add a 'B' to the end of the string and then reverse the string.
2)    
        
"A"
"ABBA"
Returns: "Impossible"
3)    
        
"AAABBAABB"
"BAABAAABAABAABBBAAAAAABBAABBBBBBBABB"
Returns: "Possible"
4)    
        
"AAABAAABB"
"BAABAAABAABAABBBAAAAAABBAABBBBBBBABB"
Returns: "Impossible"
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved. 
 */

package srm663;

import java.util.ArrayDeque;
import java.util.Queue;

import java.util.HashSet;
import java.util.Set;

public class ABBADiv1 {
  protected final static String OK = "Possible";
  protected final static String NO = "Impossible";

  public static String canObtainNaive(String initial, String target) {
    //System.out.println("canObtain("+initial+", "+target+")");
    // just in case
    if (target.equals(initial)) return OK;

    // quick check
    StringBuilder buf = new StringBuilder(initial);
    String revInit = buf.reverse().toString();
    if (!target.contains(initial) && !target.contains(revInit)) return NO;

    Set<String> visited = new HashSet<String>();

    Queue<String> q = new ArrayDeque<String>();
    q.add(initial);
    // repeat until there is a candidate
    while (!q.isEmpty()) {
      String candidate = q.poll();
      visited.add(candidate);
      //System.out.println("candidate: "+candidate);

      // try both moves, one by one
      String next1 = addA(candidate);
      if (target.equals(next1)) return OK;
      else if (next1.length() < target.length() && !visited.contains(next1))
        q.add(next1);

      String next2 = revB(candidate);
      if (target.equals(next2)) return OK;
      else if (next2.length() < target.length() && !visited.contains(next2))
        q.add(next2);
    }
    return NO;
  }

  private static String addA(String prev) {
    StringBuilder buf = new StringBuilder(prev);
    buf.append('A');
    return buf.toString();
  }

  private static String revB(String prev) {
    StringBuilder buf = new StringBuilder(prev);
    buf.append('B');
    return buf.reverse().toString();
  }

  /** key idea: if the initial string appears as-is,
   * numbers of 'B' before/after that string should be equal
   * e.g., B__BB__/init/BBB___
   * if the reversed string appears, number of 'B' before that string
   * is bigger (+1) than that of 'B' after that string
   * e.g., B__BB__/rev/__B_B_
   */
  public static String canObtain(String initial, String target) {
    // just in case
    if (target.equals(initial)) return OK;

    StringBuilder buf = new StringBuilder(initial);
    String revInit = buf.reverse().toString();
    // quick check
    if (!target.contains(initial) && !target.contains(revInit)) return NO;

    int idx = 0;
    boolean possible = false;
    while (idx < target.length() && !possible) {
      if (target.contains(initial)) {
        idx = target.indexOf(initial, idx);
      } else if (target.contains(revInit)) {
        idx = target.indexOf(revInit, idx);
      }
      if (idx < 0) break;
      String bef = target.substring(0, idx);
      String aft = target.substring(idx+initial.length(), target.length());

      int befCnt = count(bef, 'B');
      int aftCnt = count(aft, 'B');
      if (initial.equals(revInit)) {
        possible = (befCnt == aftCnt) || (befCnt == aftCnt + 1);
      }
      else if (target.contains(initial)) {
        possible = befCnt == aftCnt;
      } else if (target.contains(revInit)) {
        possible = befCnt == aftCnt + 1;
      }
      // to avoid infinite loop, advance idx
      idx++;
    }

    return possible ? OK : NO;
  }

  static int count(String str, char x) {
    int cnt = 0;
    for (char c : str.toCharArray()) {
      if (c == x) cnt++;
    }
    return cnt;
  }

}

