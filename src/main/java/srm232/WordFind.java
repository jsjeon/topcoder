/*
Problem Statement
        
You have been given a "word search" puzzle, which consists of a rectangular grid of letters, in which several words are hidden. Each word may begin anywhere in the puzzle, and may be oriented in any straight line horizontally, vertically, or diagonally. However, the words must all go down, right, or down-right. (see examples)

You are given a String[], grid, indicating the letters in the grid to be searched. Character j of element i of grid is the letter at row i, column j. You are also given a String[], wordList, indicating the words to be found in the grid. You are to return a String[] indicating the locations of each word within the grid.

The return value should have the same number of elements as wordList. Each element of wordList corresponds to the element of the return value with the same index.

Each element of the return value should be formatted as "row col" (quotes added for clarity), where row is the 0-based row in which the first letter of the word is found, and col is the 0-based column in which the first letter of the word is found. If the same word can be found more than once, the location in the lowest-indexed row should be returned. If there is still a tie, return the location with the lowest-indexed column. If a word cannot be found in the grid, return an empty string for that element.

 
Definition
        
Class:    WordFind
Method:    findWords
Parameters:    String[], String[]
Returns:    String[]
Method signature:    String[] findWords(String[] grid, String[] wordList)
(be sure your method is public)

 
Constraints
-    grid will contain between 1 and 50 elements, inclusive.
-    Each element of grid will contain between 1 and 50 characters, inclusive.
-    Each element of grid will contain the same number of characters.
-    Each character of each element of grid will be 'A'-'Z'.
-    wordList will contain between 1 and 50 elements, inclusive.
-    Each element of wordList will contain between 1 and 50 characters, inclusive.
-    Each character of each element of wordList will be 'A'-'Z'.
 
Examples
0)    

{"TEST",
 "GOAT",
 "BOAT"}
{"GOAT", "BOAT", "TEST"}
Returns: { "1 0",
  "2 0",
  "0 0" }
These words are pretty easy to find.
1)    

{"SXXX",
 "XQXM",
 "XXLA",
 "XXXR"}
{"SQL", "RAM"}
Returns: { "0 0",
  "" }
While "RAM" may be found going up at "3 3", we are only allowing words that go down and right.
2)    

{"EASYTOFINDEAGSRVHOTCJYG",
 "FLVENKDHCESOXXXXFAGJKEO",
 "YHEDYNAIRQGIZECGXQLKDBI",
 "DEIJFKABAQSIHSNDLOMYJIN",
 "CKXINIMMNGRNSNRGIWQLWOG",
 "VOFQDROQGCWDKOUYRAFUCDO",
 "PFLXWTYKOITSURQJGEGSPGG"}
{"EASYTOFIND", "DIAG", "GOING", "THISISTOOLONGTOFITINTHISPUZZLE"}
Returns: { "0 0",
  "1 6",
  "0 22",
  "" }
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.
*/

package srm232;

public class WordFind {

  static class Diag {
    int row;
    int col;
    String content;

    public Diag(int row, int col, String content) {
      this.row = row;
      this.col = col;
      this.content = content;
    }
  }

  String[] rows;
  String[] cols;
  Diag[] diags;

  public WordFind(String[] grid) {
    // m x n grid:
    // (1,1)....(1,n)
    // (2,1)....(2,n)
    // ...
    // (m,1)....(m,n)
    int m = grid.length;
    int n = grid[0].length();

    // (1,1..n)
    // (2,1..n)
    // ...
    // (m,1..n)
    rows = new String [m];
    System.arraycopy(grid, 0, rows, 0, m);

    // (1..m, 1)
    // (1..m, 2)
    // ...
    // (1..m, n)
    cols = new String [n];
    for (int i = 0; i < n; i++) {
      StringBuilder buf = new StringBuilder();
      for (int j = 0; j < m; j++) {
        buf.append(grid[j].charAt(i));
      }
      cols[i] = buf.toString();
    }

    diags = new Diag [n + m - 1];
    int idx = 0;
    // (1,1), (2,2), ... (m,m) or (n,n)
    // (1,2), (2,3), ... (m,m+1) or (n-1,n)
    // ...
    for (int i = 0; i < n; i++) {
      StringBuilder buf = new StringBuilder();
      for (int j = 0; j < m && i + j < n; j++) {
        buf.append(grid[j].charAt(i+j));
      }
      diags[idx++] = new Diag(0, i, buf.toString());
    }
    // (2,1), (3,2), ... (m, m-1) or (n-1, n)
    // (3,1), (4,2), ... (m, m-2) or (n-2, n)
    // ...
    for (int i = 1; i < m; i++) {
      StringBuilder buf = new StringBuilder();
      for (int j = 0; j < n && i + j < m; j++) {
        buf.append(grid[i+j].charAt(j));
      }
      diags[idx++] = new Diag(i, 0, buf.toString());
    }
  }

  public String findWord(String word) {
    int rowIdx = rows.length;
    int colIdx = cols.length;

    for (int j = 0; j < colIdx; j++) {
      // search cols
      String col = cols[j];
      if (col.contains(word)) {
        rowIdx = col.indexOf(word);
        colIdx = j;
        break;
      }

      // search upper-triangular diags
      Diag diag = diags[j];
      if (diag.content.contains(word)) {
        int idx = diag.content.indexOf(word);
        rowIdx = diag.row + idx;
        colIdx = diag.col + idx;
        // do not break the loop here
        // since (r,c) may be bigger by nature
      }
    }

    // now, search rows
    for (int i = 0; i < rowIdx; i++) {
      String row = rows[i];
      // low-indexed row first
      if (row.contains(word)) {
        int j = row.indexOf(word);
        return i + " " + j;
      }

      // search lower-triangular diags
      Diag diag = diags[cols.length-1+i];
      if (diag.content.contains(word)) {
        int idx = diag.content.indexOf(word);
        rowIdx = diag.row + idx;
        colIdx = diag.col + idx;
        // similarly, do not return the result immediately
      }
    }

    if (rowIdx == rows.length && colIdx == cols.length) return "";
    return rowIdx + " " + colIdx;
  }

  public static String[] findWords(String[] grid, String[] wordList) {
    WordFind finder = new WordFind(grid);
    String[] res = new String [wordList.length];
    for (int i = 0; i < wordList.length; i++) {
      res[i] = finder.findWord(wordList[i]);
    }
    return res;
  }

}

