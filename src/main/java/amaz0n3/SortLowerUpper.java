package amaz0n3;

public class SortLowerUpper {

  static boolean isLower(char c) {
    return 'a' <= c && c <= 'z';
  }

  static boolean isUpper(char c) {
    return 'A' <= c && c <= 'Z';
  }

  static String reverse(String s) {
    char[] cs = s.toCharArray();
    int len = cs.length;
    for (int i = 0; i < len / 2; i++) {
      char tmp = cs[i];
      cs[i] = cs[len-1-i];
      cs[len-1-i] = tmp;
    }
    return new String(cs);
  }

  /**
   * dutch flag algorithm doesn't preserve the order...
   */
  public static String dutch(String input) {
    if (input.length() <= 1) return input;

    char[] cs = input.toCharArray();

    int left = 0;
    int mid = 0;
    int right = input.length() - 1;
    //       left mid   right
    // ... 0 1 1  ? ? ? 2 2 2 ...

    char tmp;
    while (mid <= right) {
      char c = cs[mid];
      if (isLower(c)) {
        //         left mid right
        // ... 0 0 1 1  ? ? 2 2 2 ...
        tmp = cs[left];
        cs[left] = c;
        cs[mid] = tmp;
        left++;
        mid++;
      } else if (isUpper(c)) {
        //       left mid right
        // ... 0 1 1  ? ? 2 2 2 2 ...
        tmp = cs[right];
        cs[right] = c;
        cs[mid] = tmp;
        right--;
      } else { // space
        //        left  mid right
        // .... 0 1 1 1 ? ? 2 2 2 ...
        mid++;
      }
    }

    String separated = new String(cs);
    String[] splited = separated.split(" ");
    String lower = splited[0];
    String upper = splited[splited.length-1];
    return reverse(lower) + " " + reverse(upper);
  }

  static int specialCompare(char c1, char c2) {
    if (isLower(c1) && !isLower(c2)) return -1;
    if (!isUpper(c1) && isUpper(c2)) return -1;
    if (isLower(c2) && !isLower(c1)) return 1;
    if (!isUpper(c2) && isUpper(c1)) return 1;
    return 0;
  }

  public static String sort(String input) {
    int len = input.length();
    if (len <= 1) return input;
    
    char[] cs = input.toCharArray();

    boolean changed = true;
    while (changed) {
      changed = false;
      for (int i = 1; i <  len; i++) {
        char c1 = cs[i-1];
        char c2 = cs[i];
        if (specialCompare(c1, c2) > 0) {
          changed = true;
          cs[i-1] = c2;
          cs[i] = c1;
        }
      }
    }
    String separated = new String(cs);
    String[] splited = separated.split(" ");
    return splited[0] + " " + splited[splited.length-1];
  }

}
