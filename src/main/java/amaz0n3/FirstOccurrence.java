/*
Given a sorted array A of n elements possibly with duplicates, find the index of the first occurrence of a given number.

Eg:
Array arr = [1,2,2,2,4,5,5,6,6,6]

firstOccurrence(arr, 2) => 1
firstOccurrence(arr, 5) => 5

Array arr = [1,1,2,2,2,2,3]

firstOccurrence(arr, 2) => 2

Array arr = [1,2]

firstOccurrence(arr, 1) => 0
 */

package amaz0n3;

public class FirstOccurrence {

  public static int firstOccurrence(int[] arr, int target) {
    int len = arr.length;
    if (len <= 0) return -1;

    int left = 0;
    int right = len - 1;
    while (left < right) {
      int mid = (left + right) / 2;
      int elt = arr[mid];
      if (elt == target) {
        right = mid;
      } else if (elt < target) {
        left = mid + 1;
      } else { // elt > target
        right = mid - 1;
      }
    }
    if (arr[right] == target) return right;
    else return -1;
  }

}
