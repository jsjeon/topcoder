package amaz0n3;

import java.util.List;
import java.util.ArrayList;

public class ZigZag {

  static class Node {
    Node left;
    Node right;
    String content;

    public Node(String content) {
      this.content = content;
      left = null;
      right = null;
    }
  }

  static class NodeWithLevel {
    Node node;
    int level;

    public NodeWithLevel(Node node, int level) {
      this.node = node;
      this.level = level;
    }
  }

  public static String toString(Node root) {
    List<NodeWithLevel> q1 = new ArrayList<>();
    List<NodeWithLevel> q2 = new ArrayList<>();
    q1.add(new NodeWithLevel(root, 0));
    StringBuilder buf = new StringBuilder();
    while (!q1.isEmpty()) {
      NodeWithLevel n = q1.remove(0);
      Node node = n.node;
      buf.append(node.content);
      if (node.left != null) {
        q2.add(new NodeWithLevel(node.left, n.level+1));
      }
      if (node.right != null) {
        q2.add(new NodeWithLevel(node.right, n.level+1));
      }

      if (q1.isEmpty()) {
        for (NodeWithLevel nn : q2) {
          if (n.level % 2 == 0) {
            q1.add(nn); // queueing
          } else {
            q1.add(0, nn); // stacking
          }
        }
        q2.clear();
      }
    }
    return buf.toString();
  }

}
