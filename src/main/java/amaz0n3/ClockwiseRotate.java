package amaz0n3;

import java.util.List;
import java.util.ArrayList;

import java.util.Scanner;

public class ClockwiseRotate {

  static class Pair {
    int x;
    int y;
    public Pair(int x, int y) {
      this.x = x;
      this.y = y;
    }
  }

  static Pair newPosition(Pair p, int r) {
    if (r <= 1) return p;

    // on the edge
    if (p.x == 0 || p.x == r-1 || p.y == 0 || p.y == r-1) {
      if (p.x == 0) { // 1st row
        if (p.y < r-1) {
          return new Pair(p.x, p.y+1);
        } else { // p.y == r-1
          return new Pair(p.x+1, p.y);
        }
      } else if (p.x == r-1) { // last row
        if (p.y > 0) {
          return new Pair(p.x, p.y-1);
        } else { // p.y == 0
          return new Pair(p.x-1, p.y);
        }
      } else if (p.y == 0) { // 1st column
        if (p.x > 0) {
          return new Pair(p.x-1, p.y);
        } else { // p.x == 0
          return new Pair(p.x, p.y+1);
        }
      } else if (p.y == r-1) { // last column
        if (p.x < r-1) {
          return new Pair(p.x+1, p.y);
        } else { // p.x == r-1
          return new Pair(p.x-1, p.y);
        }
      }
    }
    // o.w. call recursively
    Pair nPos = new Pair(p.x-1, p.y-1);
    Pair nnPos = newPosition(nPos, r-2);
    nnPos.x += 1;
    nnPos.y += 1;
    return nnPos;
  }

  static int[][] clockwiseRotate(int[][] matrix, int r) {
    int[][] rotated = new int[r][r];
    for (int i = 0; i < r; i++) {
      for (int j = 0; j < r; j++) {
        Pair nPos = newPosition(new Pair(i, j), r);
        rotated[nPos.x][nPos.y] = matrix[i][j];
      }
    }
    return rotated;
  }

  static String toString(int[][] matrix, int r) {
    StringBuilder buf = new StringBuilder();
    for (int i = 0; i < r; i++) {
      for (int j = 0; j < r - 1; j++) {
        buf.append(matrix[i][j] + " ");
      }
      buf.append(matrix[i][r-1] + System.getProperty("line.separator"));
    }
    return buf.toString();
  }

  public static String rotate(String input) {
    Scanner sc = new Scanner(input);
    String line = sc.nextLine().trim();
    int r = Integer.valueOf(line);
    int c = 0;
    List<List<Integer>> matrixL = new ArrayList<>();
    for (int i = 0; i < r; i++) {
      List<Integer> row = new ArrayList<>();
      line = sc.nextLine().trim();
      String[] nStrs = line.split(" ");
      for (String nStr : nStrs) {
        int v = Integer.valueOf(nStr.trim());
        row.add(v);
      }
      c = row.size();
      if (c != r) return "ERROR";
      matrixL.add(row);
    }

    int[][] matrix = new int[r][r];
    int rIdx = 0;
    for (List<Integer> row : matrixL) {
      int cIdx = 0;
      for (Integer i : row) {
        matrix[rIdx][cIdx++] = i;
      }
      rIdx++;
    }

    if (r == 1) {
      return toString(matrix, r);
    } else {
      int[][] rotated = clockwiseRotate(matrix, r);
      return toString(rotated, r);
    }
  }

}
