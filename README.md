# TopCoder

A private (or public-to-be) repository to practice popular languages,
such as Java, by solving problems in [TopCoder][top].

[top]: https://www.topcoder.com


# Usage

To compile:

    $ gradle assemble

To test all the problems:

    $ gradle test

To test a single, specific problem:

    $ gradle test --tests Word*
    $ gradle -Dtest.single=Word* test

Test results will be stored under build/test-results/ directory.

To clean up:

    $ gradle clean

